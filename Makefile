.PHONY: all venv clean requirements

ALL_TARGETS := venv

PYTHON_BIN?=python3

all: $(ALL_TARGETS)

venv: venv/bin/activate

venv/bin/activate: requirements.txt
	test -d venv || $(PYTHON_BIN) -m venv venv
	. venv/bin/activate; pip install --upgrade pip wheel
	. venv/bin/activate; pip install -r requirements.txt
	touch venv/bin/activate

dev: venv/bin/activate
	test -d venv || $(PYTHON_BIN) -m venv venv
	. venv/bin/activate; pip install --upgrade pip wheel
	. venv/bin/activate; pip install -r requirements-ci.txt
	touch venv/bin/activate

clean:
	test -d venv && rm -rf venv || exit 0

requirements:
	rm requirements.txt
	rm requirements-ci.txt
	. venv/bin/activate; pip-compile --output-file=requirements.txt requirements.in
	. venv/bin/activate; pip-compile --output-file=requirements-ci.txt requirements.in requirements-ci.in
