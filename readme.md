# Infrastructure

## Ansible roles
### ntpserver
`ntpserver`:
Installs and configures `chrony` as NTP Server

```shell
make
source venv/bin/activate

# Test playbook
ansible-playbook --diff --check -i <dev/prod>.yml ntpserver.yml

# Production rollout
ansible-playbook -i prod.yml ntpserver.yml
```

### all
`all` installs the following roles all at once:
- `ntpserver`

```shell
make
source venv/bin/activate

# Test playbook
ansible-playbook --diff --check -i <dev/prod>.yml all.yml

# Production rollout
ansible-playbook -i prod.yml all.yml
```
